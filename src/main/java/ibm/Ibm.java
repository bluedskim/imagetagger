package ibm;

import com.ibm.watson.developer_cloud.alchemy.v1.AlchemyVision;
import com.ibm.watson.developer_cloud.alchemy.v1.model.ImageKeywords;
import com.ibm.watson.developer_cloud.visual_recognition.v3.VisualRecognition;
import com.ibm.watson.developer_cloud.visual_recognition.v3.model.*;
import google.Google;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by shed on 17. 10. 22.
 */
public class Ibm {
	String[] zipFileArr = {"zip1.zip", "zip2.zip"};

	public static void main(String[] args) throws Exception {
		long tStart = System.nanoTime();
		Ibm ibm = new Ibm();
		/*
		args = new String[]{args[0], "ko"};
		ibm.tag(args);
		args = new String[]{args[0], "en"};
		ibm.tag(args);
		*/

		args = new String[]{args[0], "ko"};
		ibm.tagBySingleImage(args);
		System.out.println("Ibm done!!! " + args[1] + " " + (System.nanoTime() - tStart) + " ns");

		args = new String[]{args[0], "en"};
		ibm.tagBySingleImage(args);
		System.out.println("Ibm done!!! " + args[1] + " " + (System.nanoTime() - tStart) + " ns");
	}

	public void tagByZip(String... args) throws Exception {
		String acceptLanguage = "ko";
		if(args.length > 1) {
			acceptLanguage = args[1];
		}
		//String imgDirectory = "/home/shed/문서/이미지 자동태깅 관련 자료/1. 순서별 이미지";
		String imgDirectory = args[0];
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());

		VisualRecognition service = new VisualRecognition(
				  VisualRecognition.VERSION_DATE_2016_05_20
		);
		service.setApiKey("561e654eaf86abac5826c1443782d73c6e653cd0");// 미국 남부

		for(String zipFileNmae : zipFileArr) {
			//InputStream imagesStream = new FileInputStream(imgDirectory + File.separator + "1. 순서별 이미지.zip");
			ClassifyOptions classifyOptions = new ClassifyOptions.Builder()
					  .acceptLanguage(acceptLanguage)
					  //.imagesFile(imagesStream)
					  .imagesFile(new File(imgDirectory + File.separator + zipFileNmae))
					  /*
					  .imagesFilename("1.zip")
					  .imagesFileContentType("application/zip")
					  */
					  //.imagesFile(new File(imgDirectory + File.separator + "001_도깨비_공유.jpg"))
					  .build();
			ClassifiedImages result = service.classify(classifyOptions).execute();
			System.out.println("getEndPoint=" + service.getEndPoint());
			System.out.println(result);

			File file = new File(imgDirectory + File.separator + "ibm" + timeStamp + "_" + zipFileNmae + "_" + acceptLanguage + ".json");
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(result.toString().getBytes());
			fos.close();
		}
		// json 을 csv로 변환 https://konklone.io/json/
	}

	public File tagBySingleImage(String... args) throws Exception {
		String imgDirectory = args[0];
		String acceptLanguage = "ko";
		if(args.length > 1) {
			acceptLanguage = args[1];
		}

		VisualRecognition service = new VisualRecognition(
				  VisualRecognition.VERSION_DATE_2016_05_20
		);
		//service.setApiKey("561e654eaf86abac5826c1443782d73c6e653cd0");// 미국 남부. 무료
		service.setApiKey("2ee4ce6500843ad5343248e0bc3b7154d91679d2");// 유료

		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		File file = new File(imgDirectory + File.separator + "ibm_" + acceptLanguage + "_" + timeStamp + "_result.csv");
		FileOutputStream fos = new FileOutputStream(file);

		List<Path> pathList = (new Google()).getPaths(imgDirectory);
		//pathList = pathList.subList(0, 1);	// 테스트로 5개만 돌려봄
		String rtn = "";
		int iter = 1;
		for(Path path : pathList) {
			System.out.println((iter++) + "/" + pathList.size() + " : " + path);
			try {
				//int i = 1/0; 강제 exception 발생
				rtn += this.tagBySingleImage(service, acceptLanguage, path.toFile());
			} catch(Exception e) {
				rtn += path.getFileName() + "\tException\t" + e + "\n";
			}
		}
		fos.write(rtn.getBytes());
		fos.close();
		return file;
	}

	public String tagBySingleImage(VisualRecognition service, String acceptLanguage, File imageFile) throws Exception {
		String out = "";

		List<ClassifiedImage> classifiedImages = getImages(service, acceptLanguage, imageFile);

		for(ClassifiedImage classifiedImage : classifiedImages) {
			for(ClassifierResult classifierResult : classifiedImage.getClassifiers()) {
				for(ClassResult classResult : classifierResult.getClasses()) {
					System.out.println("\t" + imageFile.getName() + " : " + classResult.getClassName()
							  + ", " + classResult.getScore());
					out += imageFile.getName() + "\t" + classResult.getClassName()
							  + "\t" + classResult.getScore() + "\n";
				}
			}
		}

		return out;
	}

	private List<ClassifiedImage> getImages(VisualRecognition service, String acceptLanguage, File imageFile) throws Exception {
		List<ClassifiedImage> classifiedImages = null;
		int tryCount = 5;

		for(int i = tryCount ; i > 0 ; i--) {
			try {
				ClassifyOptions classifyOptions = new ClassifyOptions.Builder()
						  .acceptLanguage(acceptLanguage)
						  //.imagesFile(imagesStream)
						  .imagesFile(imageFile)
						  /*
						  .imagesFilename("1.zip")
						  .imagesFileContentType("application/zip")
						  */
						  //.imagesFile(new File(imgPath + File.separator + "001_도깨비_공유.jpg"))
						  .build();

				ClassifiedImages result = service.classify(classifyOptions).execute();
				classifiedImages = result.getImages();
				break;
			} catch (Exception e) {
				e.printStackTrace(System.out);
				if(i == 0) {
					System.out.println(tryCount + "회 시도 하였으나 계속 오류 발생");
					throw e;
				} else {
					System.out.println("재시도 " + (i-1) + "회 남음");
				}
			}
		}

		return classifiedImages;
	}
}
