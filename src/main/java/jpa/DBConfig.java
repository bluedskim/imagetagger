package jpa;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

/**
 * Created by shed on 17. 11. 29.
 */
@Configuration
public class DBConfig{

	@Bean
	public DataSource dataSource(){
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://samba.iptime.org:9096/imagetagger?useUnicode=true&characterEncoding=utf8");
		dataSource.setUsername( "imagetagger" );
		dataSource.setPassword( "Pw2cYHPesSwDfEaJ" );
		return dataSource;
	}
}
