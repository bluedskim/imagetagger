package jpa.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import javax.persistence.*;

@Getter
@Setter
@ToString
@MappedSuperclass
public class BaseEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	/*
	@CreatedBy  // If we need to save the information of the user who created and/or modified an entity, we should use Spring Data
	private String createdUserId;

	@LastModifiedBy // If we need to save the information of the user who created and/or modified an entity, we should use Spring Data
	private String modifiedUserId;

	//@CreatedDate
	//@DateTimeFormat(style = "M-")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime createdDate;

	//@LastModifiedDate
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	//@DateTimeFormat(style = "M-")
	private DateTime modifiedDate;

	@PrePersist
	public void prePersist() {
		DateTime now = DateTime.now();
		this.createdDate = now;
		this.modifiedDate = now;
	}

	@PreUpdate
	public void preUpdate() {
		this.modifiedDate = DateTime.now();
	}

*/
  /*
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
  */
}