package amazon;

import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClientBuilder;
import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.rekognition.model.AmazonRekognitionException;
import com.amazonaws.services.rekognition.model.DetectLabelsRequest;
import com.amazonaws.services.rekognition.model.DetectLabelsResult;
import com.amazonaws.services.rekognition.model.Image;
import com.amazonaws.services.rekognition.model.Label;
import com.amazonaws.services.rekognition.model.S3Object;
import google.Google;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by shed on 17. 10. 21.
 */
public class Amazon {
	public static void main(String[] args) throws Exception {
		Amazon amazon = new Amazon();
		amazon.tag(args);
	}

	public File tag(String... args) throws Exception {
		/*
		Path path = Paths.get("/home/shed/문서/이미지 자동태깅 관련 자료/1. 순서별 이미지/001_도깨비_공유.jpg");
		byte[] data = Files.readAllBytes(path);
		ByteBuffer bf = ByteBuffer.wrap(data);

		Image image = new Image().withBytes(bf);
		*/

		String photo = "001_도깨비_공유.jpg";
		String bucket = "imageTagger";

		AWSCredentials credentials;
		try {
			//credentials = new ProfileCredentialsProvider("AdminUser").getCredentials();
			credentials = new ProfileCredentialsProvider("default").getCredentials();
		} catch(Exception e) {
			throw new AmazonClientException("Cannot load the credentials from the credential profiles file. "
					  + "Please make sure that your credentials file is at the correct "
						  + "location (/Users/userid/.aws/credentials), and is in a valid format.", e);
		}

		AmazonRekognition rekognitionClient = AmazonRekognitionClientBuilder
				  .standard()
				  .withRegion(Regions.US_WEST_2)
				  .withCredentials(new AWSStaticCredentialsProvider(credentials))
				  .build();

		//String imgDirectory = "/home/shed/문서/이미지 자동태깅 관련 자료/1. 순서별 이미지";
		String imgDirectory = args[0];
		Google google = new Google();
		List<Path> pathList = google.getPaths(imgDirectory);
		//pathList = pathList.subList(0, 1);	// 테스트로 5개만 돌려봄

		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		File file = new File(imgDirectory + File.separator + "amazon" + timeStamp + "_result.csv");
		FileOutputStream fos = new FileOutputStream(file);

		for(Path path : pathList) {
			byte[] data = Files.readAllBytes(path);
			ByteBuffer bf = ByteBuffer.wrap(data);
			String csvContents = "";
			String fileName = path.getFileName().toString();

			Image image = new Image().withBytes(bf);

			DetectLabelsRequest request = new DetectLabelsRequest()
				 /*
				  .withImage(new Image()
							 .withS3Object(new S3Object()
										.withName(photo).withBucket(bucket)))
										*/
					  .withImage(image)
					  .withMaxLabels(30)
					  .withMinConfidence(1F);

			try {
				DetectLabelsResult result = rekognitionClient.detectLabels(request);
				List <Label> labels = result.getLabels();

				System.out.println("Detected labels for " + fileName);
				if(labels.size() > 0) {
					for (Label label: labels) {
						System.out.println(label.getName() + ": " + label.getConfidence().toString());
						csvContents += fileName + "\t" + label.getName() + "\t" + label.getConfidence().toString() + "\r\n";
					}
				} else {
					// 결과가 없더라도 파일명은 저장해야
					csvContents += fileName + "\r\n";
				}
			} catch(AmazonRekognitionException e) {
				e.printStackTrace();
				csvContents += fileName + "\t" + e + "\r\n";
			}
			fos.write(csvContents.getBytes());
		}
		fos.close();
		return file;
	}
}
