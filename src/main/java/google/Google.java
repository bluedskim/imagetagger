package google;

import com.google.cloud.vision.v1.AnnotateImageRequest;
import com.google.cloud.vision.v1.AnnotateImageResponse;
import com.google.cloud.vision.v1.BatchAnnotateImagesResponse;
import com.google.cloud.vision.v1.EntityAnnotation;
import com.google.cloud.vision.v1.Feature;
import com.google.cloud.vision.v1.Feature.Type;
import com.google.cloud.vision.v1.Image;
import com.google.cloud.vision.v1.ImageAnnotatorClient;
import com.google.protobuf.ByteString;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * https://cloud.google.com/vision/docs/reference/libraries
 * Created by shed on 17. 10. 21.
 */
public class Google {
	final int maxImagesPerRequest = 16;

	public static void main(String... args) throws Exception {
		Google google = new Google();
		google.tag(args);
	}

	public File tag(String... args) throws Exception {
		setEnv();
		File file = null;
		// Instantiates a client
		try (ImageAnnotatorClient vision = ImageAnnotatorClient.create()) {
			List<AnnotateImageRequest> requests = new ArrayList<>();
			List<List<AnnotateImageRequest>> requestsBunch = new ArrayList<>();
			Feature feat = Feature.newBuilder().setType(Type.LABEL_DETECTION).build();
			//List<Path> pathList = getPaths(args[0]);
			//String imgDirectory = "/home/shed/문서/이미지 자동태깅 관련 자료/1. 순서별 이미지";
			String imgDirectory = args[0];
			List<Path> pathList = getPaths(imgDirectory);
			//pathList = pathList.subList(0, 3);	// 테스트로 5개만 돌려봄

			int iter = 0;
			for(Path path : pathList) {
				if(iter > 0 && iter % maxImagesPerRequest == 0) {
					requestsBunch.add(requests);
					requests = new ArrayList<>();
				}
				// Reads the image file into memory
				byte[] data = Files.readAllBytes(path);
				ByteString imgBytes = ByteString.copyFrom(data);

				// Builds the image annotation request
				Image img = Image.newBuilder().setContent(imgBytes).build();
				AnnotateImageRequest request = AnnotateImageRequest.newBuilder()
						  .addFeatures(feat)
						  .setImage(img)
						  .build();

				requests.add(request);
				iter++;
			}
			requestsBunch.add(requests);

			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
			file = new File(imgDirectory + File.separator + "google" + timeStamp + "_result.csv");
			FileOutputStream fos = new FileOutputStream(file);

			int bunchNum = 0;
			for(List<AnnotateImageRequest> tempRequests : requestsBunch) {
				System.out.println("tempRequests size=" + tempRequests.size());

				// Performs label detection on the image file
				BatchAnnotateImagesResponse response = vision.batchAnnotateImages(tempRequests);
				System.out.println("response=" + response.toString());

				List<AnnotateImageResponse> responses = response.getResponsesList();

				iter = 0;
				for (AnnotateImageResponse res : responses) {
					String csvContents = "";
					String fileName = pathList.get(iter + bunchNum * maxImagesPerRequest).getFileName().toString();	// 파일명
					if (res.hasError()) {
						System.out.printf("Error: %s\n", res.getError().getMessage());
						iter++;
						break;
					}

					//res.writeDelimitedTo(new FileOutputStream(file));
					//res.writeTo(fos);

					System.out.println("res.getLabelAnnotationsList()=" + res.getLabelAnnotationsList().toString()
							  //+ " getDescriptionFromJson=" + getDescriptionFromJson(res.getLabelAnnotationsList().toString())
					);

					if(res.getLabelAnnotationsList().size() > 0) {
						for (EntityAnnotation annotation : res.getLabelAnnotationsList()) {
							csvContents = fileName;
							Map map = annotation.getAllFields();
							for (Object key : map.keySet()) {
								csvContents += "\t" + map.get(key);
							}
							csvContents += "\r\n";
							fos.write(csvContents.getBytes());
						}
					} else {
						// 결과가 없더라도 파일명은 찍어야 하기 때문
						fos.write((fileName+"\r\n").getBytes());
					}
					iter++;
				}
				bunchNum++;
			}

			fos.close();
		}
		return file;
	}

	private String getDescriptionFromJson(String json){
		JSONArray results = new JSONArray(json);
		JSONObject result = (JSONObject) results.get(0);
		return result.getString("description");
	}

	public void setEnv() throws Exception {
		try {
			Map<String, String> env = System.getenv();
			Class<?> cl = env.getClass();
			Field field = cl.getDeclaredField("m");
			field.setAccessible(true);
			Map<String, String> writableEnv = (Map<String, String>) field.get(env);
			writableEnv.put("GOOGLE_APPLICATION_CREDENTIALS", "/home/shed/Utils/shell/API_Project-d1e80e8eae30.json");
		} catch (Exception e) {
			throw new IllegalStateException("Failed to set environment variable", e);
		}
	}

	public List<Path> getPaths(String directory) {
		List<Path> pathList = new ArrayList<>();

		File[] files = new File(directory).listFiles();
		for(File file : files){
			if(file.isFile()
					  && file.getName().indexOf(".csv") < 0
					  && file.getName().indexOf(".json") < 0
					  && file.getName().indexOf("output") < 0
					  && file.getName().indexOf(".zip") < 0
					  && (
					  		  file.getName().toLowerCase().indexOf(".jpg") > 0
								|| file.getName().toLowerCase().indexOf(".png") > 0
						)
				){
				//System.out.println(file.getAbsolutePath());
				pathList.add(Paths.get(file.getAbsolutePath()));
			}
		}
		System.out.println("pathList size=" + pathList.size());
		return pathList;
	}
}
