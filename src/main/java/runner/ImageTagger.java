package runner;

import amazon.Amazon;
import google.Google;
import ibm.Ibm;

import java.io.File;

/**
 * Created by shed on 17. 10. 25.
 */
public class ImageTagger {
	public static void main(String[] args) throws Exception {
		long tStart = System.nanoTime();

		Amazon amazon = new Amazon();
		File amazonRtn = amazon.tag(args);
		System.out.println("Amazon done!!! " + (System.nanoTime() - tStart) + " ns");

		tStart = System.nanoTime();
		Google google = new Google();
		File googleRtn = google.tag(args);
		System.out.println("Google done!!! " + (System.nanoTime() - tStart) + " ns");

		/*
		tStart = System.nanoTime();
		Ibm ibm = new Ibm();
		args = new String[]{args[0], "ko"};
		File ibmRtnKo = ibm.tagBySingleImage(args);
		System.out.println("Ibm done!!! " + args[1] + " " + (System.nanoTime() - tStart) + " ns");
		args = new String[]{args[0], "en"};
		File ibmRtnEn = ibm.tagBySingleImage(args);
		System.out.println("Ibm done!!! " + args[1] + " " + (System.nanoTime() - tStart) + " ns");
		*/
	}
}
