package util;

import google.Google;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by shed on 17. 11. 25.
 */
public class CsvToExcel {
	String[] googleHeader = {"google", "\t", "\t"};
	String[] amazonHeader = {"amazon", "\t"};
	String[] ibm_koHeader = {"ibm_ko", "\t"};
	String[] ibm_enHeader = {"ibm_en", "\t"};
	String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());

	public static void main(String[] args) throws Exception {
		long tStart = System.nanoTime();

		CsvToExcel csvToExcel = new CsvToExcel();
		csvToExcel.merge(args);

		System.out.println("CsvToExcel done!!! " + (System.nanoTime() - tStart) + " ns");
	}

	private void merge(String[] args) throws Exception {
		String imgDirectory = args[0];
		List<Path> imageFileList = (new Google()).getPaths(imgDirectory);
		HashMap<String, Supplier<Stream<String>>> tagSet = readTagSet(imgDirectory) ;

		int iter = 0;

		File file = new File(imgDirectory + File.separator + "allTags_" + timeStamp + ".csv");
		Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF8"));
		out.write(65279);

		for(Path image : imageFileList) {
			System.out.println((iter++) + "/" + imageFileList.size() + " : " + image);
			String singleFileOut = this.buildCsv(image, tagSet);
			System.out.println(singleFileOut);
			out.write(singleFileOut);
		}
		out.close();
	}

	// csv 파일 읽기
	private HashMap<String, Supplier<Stream<String>>> readTagSet(String imgDirectory) throws Exception {
		HashMap<String, Supplier<Stream<String>>> tagSet = new HashMap<>();

		File[] csvFiles = (new File(imgDirectory)).listFiles(
			(dir, name) -> {
				return name.toLowerCase().endsWith("_result.csv");
			}
		);

		Arrays.sort(csvFiles);

		for(File theCsv : csvFiles) {
			List<String> allLines = Files.readAllLines(Paths.get(theCsv.toURI()));
			Supplier<Stream<String>> streamSupplier = () -> allLines.stream();

			tagSet.put(theCsv.getName(), streamSupplier);
		}

		return tagSet;
	}

	private String buildCsv(Path image, HashMap<String, Supplier<Stream<String>>> tagSet) throws Exception {
		String imageFileNmae = image.toFile().getName();

		List<List<String>> tagList = new ArrayList<>();

		int maxTagCount = 0;

		// 해당 이미지의 tag만 가져오기
		for (String key : tagSet.keySet()) {
			List<String> filtered = new ArrayList<>();
			if(key.contains("google")) {
				filtered.add(String.join("", googleHeader));
			} else if(key.contains("amazon")) {
				filtered.add(String.join("", amazonHeader));
			} else if(key.contains("ibm_ko")) {
				filtered.add(String.join("", ibm_koHeader));
			} else if(key.contains("ibm_en")) {
				filtered.add(String.join("", ibm_enHeader));
			}

			List<String> tempList = tagSet.get(key).get().filter(s -> s.startsWith(imageFileNmae))
					  .collect(Collectors.toList())
					  ;

			filtered.addAll(removeFirstColumm(tempList));

			/*
			for(String theLine : filtered) {
				System.out.println(key + " : " + theLine);
			}
			*/
			tagList.add(filtered);
			if(filtered.size() > maxTagCount) {
				maxTagCount = filtered.size();
			}
		}

		fillEmpyCell(tagList, maxTagCount);

		File file = new File(image.getParent() + File.separator + imageFileNmae + ".csv");
		FileOutputStream fos = new FileOutputStream(file);
		Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF8"));
		out.write(65279);

		String allFileOut = "";
		for(int i = 0 ; i < maxTagCount ; i++) {
			String singleFileOut = imageFileNmae;
			for(List<String> tags : tagList) {
				singleFileOut += "\t";
				if(tags.size() > i) {
					singleFileOut += tags.get(i);
				}
			}
			singleFileOut += "\n";
			allFileOut += singleFileOut;
			//System.out.print(singleFileOut);
			//fos.write(singleFileOut.getBytes());
			out.write(singleFileOut);
		}
		//fos.close();
		out.close();

		return allFileOut;
	}

	private void fillEmpyCell(List<List<String>> tagList, int maxTagCount) {
		for(List<String> tags : tagList) {
			int tagSize = tags.size();
			for(int i = 0 ; i < maxTagCount - tagSize ; i++) {
				tags.add(tags.get(0).replaceAll("[a-zA-Z]|_", ""));	// 헤더를 복사해 넣음
			}
		}
	}

	private List<String> removeFirstColumm(List<String> tempList ) {
		List<String> newList = new ArrayList<>();

		for(int i = 0 ; i < tempList.size() ; i++) {
			String tempStr = tempList.get(i);
			if(tempStr.indexOf("\t") >= 0) {
				newList.add(tempStr.substring(tempStr.indexOf("\t") + 1));
			}
		}
		return newList;
	}
}
